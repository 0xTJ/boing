#include <stdio.h>
#include <stdint.h>
#include <stdalign.h>
#include <stdbool.h>
#include <string.h>
#include <machine.h>
#include "v99x8.h"
#include "util.h"

void draw_line(long x_0, long y_0, long x_1, long y_1, uint8_t colour) {
    enum v99x8_argument arg = V99X8_ARGUMENT_MXD_VRAM;

    long maj = x_1 - x_0;
    long min = y_1 - y_0;

    if (maj < 0) {
        maj = -maj;
        arg |= V99X8_ARGUMENT_DIX_LEFT;
    } else {
        arg |= V99X8_ARGUMENT_DIX_RIGHT;
    }

    if (min < 0) {
        min = -min;
        arg |= V99X8_ARGUMENT_DIY_UP;
    } else {
        arg |= V99X8_ARGUMENT_DIY_DOWN;
    }

    if (maj < min) {
        long tmp = maj;
        maj = min;
        min = tmp;
        arg |= V99X8_ARGUMENT_MAJ_LONG_Y;
    } else {
        arg |= V99X8_ARGUMENT_MAJ_LONG_X;
    }

    v99x8_line(x_0, y_0, maj, min, colour, arg, V99X8_LOGICAL_OPERATION_IMP);
}

void draw_rectangle(long x_0, long y_0, long x_1, long y_1, uint8_t colour) {
    enum v99x8_argument arg = V99X8_ARGUMENT_MXD_VRAM;

    long nx = x_1 - x_0;
    long ny = y_1 - y_0;

    if (nx < 0) {
        nx = -nx;
        arg |= V99X8_ARGUMENT_DIX_LEFT;
    } else {
        arg |= V99X8_ARGUMENT_DIX_RIGHT;
    }

    if (ny < 0) {
        ny = -ny;
        arg |= V99X8_ARGUMENT_DIY_UP;
    } else {
        arg |= V99X8_ARGUMENT_DIY_DOWN;
    }

    nx += 1;
    ny += 1;

    v99x8_lmmv(x_0, y_0, nx, ny, colour, arg, V99X8_LOGICAL_OPERATION_IMP);
}

static inline long scale_coord(long v, long scale, long v_center, long scale_center) {
    return (v - v_center) * scale / scale_center + v_center;
}

void draw_perspective_line(long x_0, long y_0, long scale_0, long x_1, long y_1, long scale_1, long x_center, long y_center, long scale_center, uint8_t colour) {
    long persp_x_0 = scale_coord(x_0, scale_0, x_center, scale_center);
    long persp_y_0 = scale_coord(y_0, scale_0, y_center, scale_center);
    long persp_x_1 = scale_coord(x_1, scale_1, x_center, scale_center);
    long persp_y_1 = scale_coord(y_1, scale_1, y_center, scale_center);
    draw_line(persp_x_0, persp_y_0, persp_x_1, persp_y_1, colour);
}

void draw_perspective_rectangle(long x_0, long y_0, long x_1, long y_1, long scale, long x_center, long y_center, long scale_center, uint8_t colour) {
    long persp_x_0 = scale_coord(x_0, scale, x_center, scale_center);
    long persp_y_0 = scale_coord(y_0, scale, y_center, scale_center);
    long persp_x_1 = scale_coord(x_1, scale, x_center, scale_center);
    long persp_y_1 = scale_coord(y_1, scale, y_center, scale_center);
    draw_rectangle(persp_x_0, persp_y_0, persp_x_1, persp_y_1, colour);
}

void bitmap_to_spritemap_16(const uint16_t bitmap[16], uint8_t spritemap[4][8]) {
    for (unsigned block = 0; block < 4; ++block) {
        for (unsigned row_in_block = 0; row_in_block < 8; ++row_in_block) {
            unsigned effective_row = row_in_block + (block & 0x1 ? 8 : 0);
            spritemap[block][row_in_block] = (bitmap[effective_row] >> (block & 0x2 ? 0 : 8)) & 0xFF;
        }
    }
}

void bitmap_to_spritemap_32(const uint32_t bitmap[32], uint8_t spritemap[4][4][8]) {
    for (unsigned superblock = 0; superblock < 4; ++superblock) {
        for (unsigned block = 0; block < 4; ++block) {
            for (unsigned row_in_block = 0; row_in_block < 8; ++row_in_block) {
                unsigned effective_row = row_in_block + (block & 0x1 ? 8 : 0) + (superblock & 0x1 ? 16 : 0);
                spritemap[superblock][block][row_in_block] = (bitmap[effective_row] >> (block & 0x2 ? 0 : 8) >> (superblock & 0x2 ? 0 : 16)) & 0xFF;
            }
        }
    }
}

uint16_t cyl_bitmap_16_half_line(uint16_t val) {
    val >>= 8;

    val = (((val >> 2) & 0xF) << 4) | (((val >> 0) & 0x7) << 1) | (((val >> 0) & 0x1) << 0);

    val <<= 8;
    return val;
}

void cyl_bitmap_16(uint16_t bitmap[16]) {
    for (unsigned line = 0; line < 16; ++line) {
        bitmap[line] = cyl_bitmap_16_half_line(bitmap[line]) | breverse_u16(cyl_bitmap_16_half_line(breverse_u16(bitmap[line])));
    }
}

uint16_t cir_bitmap_16_half_line(uint16_t val, unsigned line) {
    if (line >= 8) {
        line = 15 - line;
    }
    val >>= 8;

    switch (line) {
    case 0:
        val = ((val >> 5) & 0x1) << 2 | ((val >> 3) & 0x1) << 1 | ((val >> 0) & 0x1) << 0;
        break;
    case 1:
        val = ((val >> 5) & 0x3) << 3 | ((val >> 2) & 0x3) << 1 | ((val >> 0) & 0x1) << 0;
        break;
    case 2:
        val = ((val >> 7) & 0x1) << 5 | ((val >> 3) & 0x7) << 2 | ((val >> 0) & 0x3) << 0;
        break;
    case 3:
    case 4:
        val = ((val >> 5) & 0x7) << 4 | ((val >> 0) & 0xF) << 0;
        break;
    default:
        break;
    }

    val <<= 8;
    return val;
}

void cir_bitmap_16(uint16_t bitmap[16]) {
    for (unsigned line = 0; line < 16; ++line) {
        bitmap[line] = cir_bitmap_16_half_line(bitmap[line], line) | breverse_u16(cir_bitmap_16_half_line(breverse_u16(bitmap[line]), line));
    }
}

uint32_t cyl_bitmap_32_half_line(uint32_t val) {
    val >>= 16;

    val = (((val >> 12) & 0x1) << 15) | (((val >> 6) & 0x1F) << 10) | (((val >> 4) & 0x7) << 7) | (((val >> 2) & 0x7) << 4) | (((val >> 0) & 0x7) << 1) | (((val >> 0) & 0x1) << 0);

    val <<= 16;
    return val;
}

void cyl_bitmap_32(uint32_t bitmap[32]) {
    for (unsigned line = 0; line < 32; ++line) {
        bitmap[line] = cyl_bitmap_32_half_line(bitmap[line]) | breverse_u32(cyl_bitmap_32_half_line(breverse_u32(bitmap[line])));
    }
}

uint32_t cir_bitmap_32_half_line(uint32_t val, unsigned line) {
    if (line >= 16) {
        line = 31 - line;
    }
    val >>= 16;

    switch (line) {
    case 0:
        val = ((val >> 12) & 0x1) << 3 | ((val >> 8) & 0x1) << 2 | ((val >> 4) & 0x1) << 1 | ((val >> 0) & 0x1) << 0;
        break;
    case 1:
        val = ((val >> 14) & 0x1) << 6 | ((val >> 11) & 0x1) << 5 | ((val >> 9) & 0x1) << 4 | ((val >> 7) & 0x1) << 3 | ((val >> 5) & 0x1) << 2 | ((val >> 2) & 0x1) << 1 | ((val >> 0) & 0x1) << 0;
        break;
    case 2:
        val = ((val >> 14) & 0x1) << 8 | ((val >> 11) & 0x3) << 6 | ((val >> 9) & 0x1) << 5 | ((val >> 7) & 0x1) << 4 | ((val >> 4) & 0x3) << 2 | ((val >> 2) & 0x1) << 1 | ((val >> 0) & 0x1) << 0;
        break;
    case 3:
        val = ((val >> 13) & 0x3) << 8 | ((val >> 10) & 0x3) << 6 | ((val >> 8) & 0x1) << 5 | ((val >> 5) & 0x3) << 3 | ((val >> 2) & 0x3) << 1 | ((val >> 0) & 0x1) << 0;
        break;
    case 4:
        val = ((val >> 15) & 0x1) << 10 | ((val >> 12) & 0x3) << 8 | ((val >> 9) & 0x3) << 6 | ((val >> 6) & 0x3) << 4 | ((val >> 3) & 0x3) << 2 | ((val >> 0) & 0x3) << 0;
        break;
    case 5:
        val = ((val >> 15) & 0x1) << 11 | ((val >> 11) & 0x7) << 8 | ((val >> 7) & 0x7) << 5 | ((val >> 3) & 0x7) << 2 | ((val >> 0) & 0x3) << 0;
        break;
    case 6:
        val = ((val >> 14) & 0x3) << 11 | ((val >> 9) & 0xF) << 7 | ((val >> 4) & 0xF) << 3 | ((val >> 0) & 0x7) << 0;
        break;
    case 7:
    case 8:
        val = ((val >> 13) & 0x7) << 11 | ((val >> 5) & 0x7F) << 4 | ((val >> 0) & 0xF) << 0;
        break;
    case 9:
    case 10:
    case 11:
        val = ((val >> 9) & 0x7F) << 8 | ((val >> 0) & 0xFF) << 0;
        break;
    default:
        break;
    }

    val <<= 16;
    return val;
}

void cir_bitmap_32(uint32_t bitmap[32]) {
    for (unsigned line = 0; line < 32; ++line) {
        bitmap[line] = cir_bitmap_32_half_line(bitmap[line], line) | breverse_u32(cir_bitmap_32_half_line(breverse_u32(bitmap[line]), line));
    }
}

void do_v(void) {
    unsigned char id = (v99x8_status_register_1_read() & V99X8_STATUS_REGISTER_1_ID_MASK) >> V99X8_STATUS_REGISTER_1_ID_SHIFT;
    printf("Video ID: %hu\n", id);

    if (id == V99X8_ID_V9958) {
        printf("Attached video chip is V9958\n");

        // Set mode G7, disable interrupts and enable output
        v99x8_mode_r0_write(V99X8_MODE_R0_M5 | V99X8_MODE_R0_M4 | V99X8_MODE_R0_M3);
        v99x8_mode_r1_write(V99X8_MODE_R1_BL | V99X8_MODE_R1_SI);
        // v99x8_mode_r1_write(V99X8_MODE_R1_BL);

        // Set PAL mode
        v99x8_mode_r9_write(V99X8_MODE_R9_LN | V99X8_MODE_R9_NT);
        v99x8_text_and_screen_margin_color_write(0x00);
        // v99x8_mode_r8_write(V99X8_MODE_R8_VR | V99X8_MODE_R8_SPD);
        v99x8_mode_r8_write(V99X8_MODE_R8_VR);

        // Set pattern layout table to 0x0
        v99x8_pattern_layout_table_write((0x0000 >> 11) | 0x1F);

        // Set sprite pattern generator table to 0xF000
        v99x8_sprite_pattern_generator_table_write((0xF000 >> 11) | 0x00);

        // Set sprite attribute table to 0xFA00
        v99x8_sprite_attribute_table_write((0xFA00 >> 7) | 0x03);

        // Set top line displayed to 0
        v99x8_vertical_offset_register_write(0);

        // Clear display
        v99x8_lmmv(0, 0, 256, 212, v99x8_rgb3x8_to_grb8(170, 170, 170), V99X8_ARGUMENT_MXD_VRAM | V99X8_ARGUMENT_DIY_DOWN | V99X8_ARGUMENT_DIX_RIGHT, V99X8_LOGICAL_OPERATION_IMP);

        // Clear sprites
        uint8_t cleared_4_pattern[4][8] = { 0 };
        v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 0, cleared_4_pattern);
        for (int sprite = 0; sprite < 32; ++sprite) {
            v99x8_sm2_sprite_attribute_write(0xFA00, sprite, 216, 0, 0);
        }

        uint32_t filled[32];
        for (unsigned row = 0; row < 32; ++row) {
            filled[row] = 0xFFFFFFFF;
        }
        cyl_bitmap_32(filled);
        cir_bitmap_32(filled);
        uint8_t filled_sprite[4][4][8];
        bitmap_to_spritemap_32(filled, filled_sprite);

        uint32_t dithered_filled[32];
        for (unsigned row = 0; row < 32; ++row) {
            dithered_filled[row] = row & 0x1 ? 0x55555555 : 0xAAAAAAAA;
            dithered_filled[row] &= filled[row];
        }
        uint8_t dithered_filled_sprite[4][4][8];
        bitmap_to_spritemap_32(dithered_filled, dithered_filled_sprite);

        uint8_t red[16];
        uint8_t white[16];
        uint8_t grey[16];
        for (int j = 0; j < 16; ++j) {
            red[j] = 0xA;
            white[j] = 0xF;
            grey[j] = 0x7;
        }
        v99x8_sm2_sprite_color_write(0xFA00, 0, true, white);
        v99x8_sm2_sprite_color_write(0xFA00, 1, true, white);
        v99x8_sm2_sprite_color_write(0xFA00, 2, true, white);
        v99x8_sm2_sprite_color_write(0xFA00, 3, true, white);
        v99x8_sm2_sprite_color_write(0xFA00, 4, true, red);
        v99x8_sm2_sprite_color_write(0xFA00, 5, true, red);
        v99x8_sm2_sprite_color_write(0xFA00, 6, true, red);
        v99x8_sm2_sprite_color_write(0xFA00, 7, true, red);
        v99x8_sm2_sprite_color_write(0xFA00, 8, true, grey);
        v99x8_sm2_sprite_color_write(0xFA00, 9, true, grey);
        v99x8_sm2_sprite_color_write(0xFA00, 10, true, grey);
        v99x8_sm2_sprite_color_write(0xFA00, 11, true, grey);

        long pos_x = 0;
        long pos_y = 100;

        long vel_x = 2;
        // long vel_x = 0;
        long vel_y = 0;

        long acc_x = 0;
        // long acc_y = -1;

        long wall_xn = 20;
        long wall_xp = 220;
        long wall_yn = 20;
        long wall_yp = 190;

        long sprite_size = 16;
        long object_size = sprite_size * 2;

        long dtime = 1;

        bool wall_xn_bounces = true;
        bool wall_xp_bounces = true;
        bool wall_yn_bounces = false;
        bool wall_yp_bounces = true;
        
        long scaling = 128;

        long pos_x_scaled = pos_x * scaling;
        long pos_y_scaled = pos_y * scaling;

        long vel_x_scaled = vel_x * scaling;
        long vel_y_scaled = vel_y * scaling;

        long acc_x_scaled = acc_x * scaling;
        long acc_y_scaled = 50;
        // long acc_y_scaled = 0;

        long wall_xn_scaled = wall_xn * scaling;
        long wall_xp_scaled = wall_xp * scaling;
        long wall_yn_scaled = wall_yn * scaling;
        long wall_yp_scaled = wall_yp * scaling;

        long object_size_scaled = object_size * scaling;

        long scale_front = 36;
        long scale_mid = 32;
        long scale_back = 28;

        long mid_x = (wall_xp - wall_xn) / 2 + wall_xn;
        long mid_y = (wall_yp - wall_yn) / 2 + wall_yn;

        for (long this_y = wall_yn; this_y <= wall_yp; this_y += 10) {
            draw_perspective_line(wall_xn, this_y, scale_back, wall_xp, this_y, scale_back, mid_x, mid_y, scale_mid, v99x8_rgb3x8_to_grb8(170, 0, 170));
        }
        for (long this_x = wall_xn; this_x <= wall_xp; this_x += 10) {
            draw_perspective_line(this_x, wall_yn, scale_back, this_x, wall_yp, scale_back, mid_x, mid_y, scale_mid, v99x8_rgb3x8_to_grb8(170, 0, 170));
        }
        for (long this_scale = scale_front; this_scale > scale_back; --this_scale) {
            // draw_perspective_line(wall_xn, wall_yn, this_scale, wall_xp, wall_yn, this_scale, mid_x, mid_y, scale_mid, 0x1F);
            // draw_perspective_line(wall_xp, wall_yn, this_scale, wall_xp, wall_yp, this_scale, mid_x, mid_y, scale_mid, 0x1F);
            draw_perspective_line(wall_xp, wall_yp, this_scale, wall_xn, wall_yp, this_scale, mid_x, mid_y, scale_mid, v99x8_rgb3x8_to_grb8(170, 0, 170));
            // draw_perspective_line(wall_xn, wall_yp, this_scale, wall_xn, wall_yn, this_scale, mid_x, mid_y, scale_mid, 0x1F);
        }
        for (long this_x = wall_xn; this_x <= wall_xp; this_x += 10) {
            // draw_perspective_line(this_x, wall_yn, scale_front, this_x, wall_yn, scale_back, mid_x, mid_y, scale_mid, 0x1F);
            draw_perspective_line(this_x, wall_yp, scale_front, this_x, wall_yp, scale_back, mid_x, mid_y, scale_mid, v99x8_rgb3x8_to_grb8(170, 0, 170));
        }
        for (long this_y = wall_yn; this_y <= wall_yp; this_y += 10) {
            // draw_perspective_line(wall_xn, this_y, scale_front, wall_xn, this_y, scale_back, mid_x, mid_y, scale_mid, 0x1F);
            // draw_perspective_line(wall_xp, this_y, scale_front, wall_xp, this_y, scale_back, mid_x, mid_y, scale_mid, 0x1F);
        }

        v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 0, filled_sprite[0]);
        v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 4, filled_sprite[1]);
        v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 8, filled_sprite[2]);
        v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 12, filled_sprite[3]);
        v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 32, dithered_filled_sprite[0]);
        v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 36, dithered_filled_sprite[1]);
        v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 40, dithered_filled_sprite[2]);
        v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 44, dithered_filled_sprite[3]);

        unsigned short rotation = 0;
        for (unsigned long time = 0; ; time += dtime) {
            if (pos_x_scaled <= wall_xn_scaled) {
                pos_x_scaled = wall_xn_bounces * (wall_xn_scaled - pos_x_scaled) + wall_xn_scaled + scaling;
                vel_x_scaled = wall_xn_bounces * (-vel_x_scaled);
            }
            if (pos_x_scaled + object_size_scaled  >= wall_xp_scaled) {
                pos_x_scaled = wall_xp_bounces * (wall_xp_scaled - pos_x_scaled - object_size_scaled) + wall_xp_scaled - scaling - object_size_scaled;
                vel_x_scaled = wall_xp_bounces * (-vel_x_scaled);
            }
            if (pos_y_scaled <= wall_yn_scaled) {
                pos_y_scaled = wall_yn_bounces * (wall_yn_scaled - pos_y_scaled) + wall_yn_scaled + scaling;
                vel_y_scaled = wall_yn_bounces * (-vel_y_scaled);
            }
            if (pos_y_scaled + object_size_scaled >= wall_yp_scaled) {
                pos_y_scaled = wall_yp_bounces * (wall_yp_scaled - pos_y_scaled - object_size_scaled) + wall_yp_scaled - scaling - object_size_scaled;
                vel_y_scaled = wall_yp_bounces * (-vel_y_scaled);
            }

            if (time % 4 == 0) {
                if (vel_x_scaled > 0) {
                    rotation -= 1;
                } else if (vel_x_scaled < 0) {
                    rotation += 1;
                }
                rotation %= 8;
            }
            uint32_t bars[32];
            for (unsigned row = 0; row < 32; ++row) {
                bars[row] = (0xF0F0F0F0F0 >> rotation) ^ (row & 0x8 ? 0xFFFFFFFF : 0);
            }
            cyl_bitmap_32(bars);
            cir_bitmap_32(bars);
            uint8_t bars_sprite[4][4][8];
            bitmap_to_spritemap_32(bars, bars_sprite);

            uint8_t sprite_pos_x = pos_x_scaled / scaling;
            uint8_t sprite_pos_y = pos_y_scaled / scaling;
            uint8_t shadow_pos_x = sprite_pos_x + 10;
            uint8_t shadow_pos_y = sprite_pos_y + 0;

            v99x8_sm2_sprite_attribute_write(0xFA00, 0, sprite_pos_y, sprite_pos_x, 16);
            v99x8_sm2_sprite_attribute_write(0xFA00, 1, sprite_pos_y + sprite_size, sprite_pos_x, 20);
            v99x8_sm2_sprite_attribute_write(0xFA00, 2, sprite_pos_y, sprite_pos_x + sprite_size, 24);
            v99x8_sm2_sprite_attribute_write(0xFA00, 3, sprite_pos_y + sprite_size, sprite_pos_x + sprite_size, 28);
            v99x8_sm2_sprite_attribute_write(0xFA00, 4, sprite_pos_y, sprite_pos_x, 0);
            v99x8_sm2_sprite_attribute_write(0xFA00, 5, sprite_pos_y + sprite_size, sprite_pos_x, 4);
            v99x8_sm2_sprite_attribute_write(0xFA00, 6, sprite_pos_y, sprite_pos_x + sprite_size, 8);
            v99x8_sm2_sprite_attribute_write(0xFA00, 7, sprite_pos_y + sprite_size, sprite_pos_x + sprite_size, 12);
            v99x8_sm2_sprite_attribute_write(0xFA00, 8, shadow_pos_y, shadow_pos_x, 32);
            v99x8_sm2_sprite_attribute_write(0xFA00, 9, shadow_pos_y + sprite_size, shadow_pos_x, 36);
            v99x8_sm2_sprite_attribute_write(0xFA00, 10, shadow_pos_y, shadow_pos_x + sprite_size, 40);
            v99x8_sm2_sprite_attribute_write(0xFA00, 11, shadow_pos_y + sprite_size, shadow_pos_x + sprite_size, 44);

            v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 16, bars_sprite[0]);
            v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 20, bars_sprite[1]);
            v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 24, bars_sprite[2]);
            v99x8_sm2_sprite_pattern_generator_write_4(0xF000, 28, bars_sprite[3]);

            pos_x_scaled += vel_x_scaled * dtime / 2;
            pos_y_scaled += vel_y_scaled * dtime / 2;
            vel_x_scaled += acc_x_scaled * dtime;
            vel_y_scaled += acc_y_scaled * dtime;
            pos_x_scaled += vel_x_scaled * dtime / 2;
            pos_y_scaled += vel_y_scaled * dtime / 2;
        }
    } else {
        printf("Attached video chip is not V9958\n");
    }
}

void kmain() {  
    printf("Starting Mosys\n");

    do_v();
}
